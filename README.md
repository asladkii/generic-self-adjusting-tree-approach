# Warning: CLONING requires a special command!

Cloning this repo is complicated by the existence of *submodules*:

```shell
git clone https://gitlab.com/asladkii/generic-self-adjusting-tree-approach.git --recurse-submodules
```

Note: if you check out a branch, you must run `git submodule update` to pull the correct versions of the submodules for that branch.

# Project Structure

The project consists of two parts:
1. [Implementation of Generic Self-Adjusting Trees (GSATs)](gsat/)
2. [Tool for benchmarking and plotting results](microbench/)
