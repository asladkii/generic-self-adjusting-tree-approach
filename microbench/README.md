# Benchmarking Tool 

Main benchmarking tool is forked from [setbench](https://gitlab.com/Mr_Ravil/setbench).

## Dependencies installation

### Setting up SetBench on Ubuntu 20.04 or 18.04

Installing necessary build tools, libraries and python packages:

```shell
sudo apt install build-essential make g++ git time libnuma-dev numactl dos2unix parallel python3 python3-pip zip
pip3 install numpy matplotlib pandas seaborn ipython ipykernel jinja2 colorama
```

Installing LibPAPI (needed for per-operation cache-miss counts, cycle counts, etc.):

```shell
git clone https://github.com/icl-utk-edu/papi
cd papi/src
./configure
sudo sh -c "echo 2 > /proc/sys/kernel/perf_event_paranoid"
./configure
make -j
make test
sudo make install
sudo ldconfig
```

### Setting up SetBench on Ubuntu 16.04

Installing necessary build tools, libraries and python packages:

```shell
sudo apt update
sudo apt install build-essential make g++ git time libnuma-dev numactl dos2unix parallel python3 python3-pip zip
pip3 install --upgrade pip
pip3 install numpy matplotlib pandas seaborn ipython ipykernel jinja2 colorama
```

*The rest is the same as in Ubuntu 18.04+ (above).*

### Setting up SetBench on Fedora 32

Installing necessary build tools, libraries and python packages:

```shell
dnf update -y
dnf install -y @development-tools dos2unix gcc g++ git make numactl numactl-devel parallel python3 python3-pip time findutils hostname zip perf papi papi-static papi-libs
pip3 install numpy matplotlib pandas seaborn ipython ipykernel jinja2 colorama
```

*The rest is the same as in Ubuntu 18.04+ (above).*

### Other operating systems

- Debian: Should probably work, as it's very similar to Ubuntu... may need some minor tweaks to package names (can usually be resolved by googling "debian cannot find package xyz").

- Windows (WSL): Should work if you disable `USE_PAPI` in the Makefile(s), and eliminate any mention of `numactl`. Note that hardware counters won't work.

- FreeBSD: Could possibly make this work with a lot of tweaking.

- Mac / OSX: Suspect it should work with minor tweaking, but haven't tested.

### Other architectures

This benchmark is for Intel/AMD x86_64.

It likely needs new memory fence placements, or a port to use `atomics`, if it is to run correctly on ARM or POWER (except in x86 memory model emulation mode).

## Preparing data structures

To prepare data structure for benchmarking:
1. Create an adapter (`adapter.h`) for it 
2. Put directory containing it it inside [ds](../ds/)

To build all data structure inside [ds](../ds/) use the following command:

```shell
cd generic-self-adjusting-tree-approach/microbench
make -j
```

## Running benchmarks

Now we can run benchmarks for built data structures. Commands to launch benchmark have the following pattern:

```shell
cd generic-self-adjusting-tree-approach/microbench

LD_PRELOAD=../lib/libjemalloc.so ./bin/<data_structure_name>.debra <workload_type> <parameters>
```

Supported workloads as well as their parameters described [here](./WORKLOADS.md).

Run example:

```shell
LD_PRELOAD=../lib/libjemalloc.so ./bin/aksenov_splaylist_64.debra -skewed-sets 
    -rp 0.9 -rs 0.1 -wp 0.9 -ws 0.2 -inter 0.05 -i 0.05 -d 0.05 -rq 0 -k 100000 
    -prefillsize 100000 -nprefill 8 -t 10000 -nrq 0 -nwork 8 -prefill-insert 
```

### Troubleshooting

If something breaks after the launch, or there is such a problem:

```shell
PAPI ERROR: thread 0 unable to add event PAPI_L2_DCM: Permission level does not permit operation
```

then the following can help:

```shell
sudo sysctl kernel.perf_event_paranoid=1
```

## Plotting results

If instead of command line output you want to see graphs with the results - go [here](../plotting/).
