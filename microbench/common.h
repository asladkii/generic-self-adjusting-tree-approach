//
// Created by Ravil Galiev on 28.02.2023.
//

#ifndef SETBENCH_COMMON_H
#define SETBENCH_COMMON_H

#include <string>


#include "distributions/distribution.h"

#include "distributions/zipf_distribution.h"
#include "distributions/uniform_distribution.h"
#include "distributions/skewed_sets_distribution.h"

#include "distributions/parameters/distribution_parameters.h"
#include "distributions/builder/distribution_builder.h"

#include "parameters.h"

#include "key_generators/key_generator.h"

#include "parameters_parser.h"

//#include "distributions/parameters/skewed_set_parameters.h"
//#include "distributions/parameters/zipf_parameters.h"






//#include "key_generators/key_generator_impls.h"

//#include "key_generators/data/creakers_and_wave_key_generator_data.h"
//#include "key_generators/data/skewed_sets_key_generator_data.h"
//#include "key_generators/data/temporary_skewed_key_generator_data.h"
//
//#include "key_generators/simple_key_generator.h"
//#include "key_generators/skewed_sets_key_generator.h"
//#include "key_generators/temporary_skewed_key_generator.h"
//#include "key_generators/creakers_and_wave_key_generator.h"




#endif //SETBENCH_COMMON_H
