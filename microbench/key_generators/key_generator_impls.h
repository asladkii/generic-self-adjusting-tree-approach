//
// Created by Ravil Galiev on 03.03.2023.
//

#ifndef SETBENCH_KEY_GENERATOR_IMPLS_H
#define SETBENCH_KEY_GENERATOR_IMPLS_H

#include "creakers_and_wave_key_generator.h"
#include "simple_key_generator.h"
#include "skewed_sets_key_generator.h"
#include "temporary_skewed_key_generator.h"

#endif //SETBENCH_KEY_GENERATOR_IMPLS_H
