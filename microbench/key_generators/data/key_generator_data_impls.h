//
// Created by Ravil Galiev on 03.03.2023.
//

#ifndef SETBENCH_KEY_GENERATOR_DATA_IMPLS_H
#define SETBENCH_KEY_GENERATOR_DATA_IMPLS_H

#include "creakers_and_wave_key_generator_data.h"
#include "skewed_sets_key_generator_data.h"
#include "temporary_skewed_key_generator_data.h"

#endif //SETBENCH_KEY_GENERATOR_DATA_IMPLS_H
