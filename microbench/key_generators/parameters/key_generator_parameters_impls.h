//
// Created by Ravil Galiev on 02.03.2023.
//

#ifndef SETBENCH_KEY_GENERATOR_PARAMETERS_IMPLS_H
#define SETBENCH_KEY_GENERATOR_PARAMETERS_IMPLS_H

#include "creakers_and_wave_parameters.h"
#include "simple_parameters.h"
#include "skewed_sets_parameters.h"
#include "temporary_skewed_parameters.h"

#endif //SETBENCH_KEY_GENERATOR_PARAMETERS_IMPLS_H
