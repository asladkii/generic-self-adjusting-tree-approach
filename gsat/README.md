# Generic Self-Adjusting Trees (GSATs)

This directory contains all files related to running, testing and building GSATs. 

## Dependencies installation

Need: gcc compiler (version at least 11.2), cmake and tbb.
Optional: gdb - debugger, ninja - alternative to make, clang-format and cleantidy - utilities for checking for compliance with the code style guide.

* **Ubuntu**:
    ```shell
    sudo apt-get install g++ git gdb cmake ninja-build clang-format clang-tidy libtbb-dev
    ```
* **OSX**:
    ```shell
    brew install gcc install cmake
    ```
* **Windows**: install [WSL](https://learn.microsoft.com/en-us/windows/wsl/install) and follow instruction for Ubuntu.

## Structure

```
├── cmake
├── common
├── ds
│   ├── btree
│   ├── gsat
│   ├── ist
│   ├── sabt
│   ├── sait
│   ├── salt
│   ├── sast
│   └── splay_tree
└── test
    ├── catch
    ├── map
    └── util
```

[ds](./ds/) contains trees implementations. Each directory `ds/<tree_name>` has the following files:
- `<tree_name>_node.h` --- tree's node implementation
- `<tree_name>.h` --- tree implementation
- `<test.cpp>` --- script for testing data structure (optional). Internally, it uses [catch2](https://github.com/catchorg/Catch2) unit testing framework, so must use corresponding [command line arguments](https://github.com/catchorg/Catch2/blob/devel/docs/command-line.md).

Each `test.cpp` file has the following test cases:
- `insert_delete` --- simple unit-test for insert, delete and contains ordered set operations
- `<size>_stress` --- stress-test. `<size>` reflects how long the test will run. Each test will run about for about this time: `small_stress` ~ 100 ms, `mid_stress` ~ 1 min, `big_<type>_stress` ~ 2 hours.

## Implemented Trees

The following data structures are currently implemented:

GSATs:
- [Self-Adjusting Interpolation Tree (SAIT)](ds/sait/)
- [Self-Adjusting B-Tree (SABT)](ds/sabt/)
- [Self-Adjusting Log Tree (SALT)](ds/salt/)
- [Self-Adjusting Single Tree (SAST)](ds/sast/)

Self-Adjusting Trees:
- [Splay Tree](ds/splay_tree/)

Non-Self-Adjusting Trees:
- [B-Tree](ds/btree/)
- [Interpolation Search Tree (IST)](ds/ist/)

## Building && Laucnhing tests

It is assumed that you are at the root of the project.

1. Building cmake:
    ```shell
    cmake -DCMAKE_PROGRAM=ninja -DCMAKE_BUILD_TYPE=ASAN -G Ninja -S . -B cmake-build
    ```
2. Building tests for `<tree_name>`:
    ```shell
    cmake --build cmake-build --target test_<tree_name> -j
    ```
3. Running `<test_case_name>`:
    ```shell
    cmake-build/gsat/ds/<tree_name>/test_<tree_name> -d yes <test_case_name>
    ```

To run all test cases, use the following command:
```shell
cmake-build/gsat/ds/<tree_name>/test_<tree_name> -d yes --order decl
```

### Example

For running mid stress-test for [SAIT](ds/sait/) run the following commands from the root of the project:
```shell
cmake -DCMAKE_PROGRAM=ninja -DCMAKE_BUILD_TYPE=ASAN -G Ninja -S . -B cmake-build
cmake --build cmake-build --target test_sait -j
cmake-build/gsat/ds/sait/test_sait -d yes mid_stress
```
